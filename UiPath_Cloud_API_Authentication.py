import json
import requests
import time
import traceback
import os
import tkinter
from tkinter.filedialog import askopenfilename


def refresh_token_data(refresh_token):
    url = 'https://account.uipath.com/oauth/token'
    refresh_token_data = {
        "grant_type": "refresh_token",
        "client_id": "5v7PmPJL6FOGu6RB8I1Y4adLBhIwovQN",
        "refresh_token": refresh_token
    }
    response = requests.post(url, data=refresh_token_data)
    print(response)
    response_json = json.loads(response.text)

    ##    auth_dict['id_token'] = response_json['id token']
    ##    auth_dict['access_token'] = response_json['access token']
    ##    auth_dict['refresh_token'] = response_json['refresh token']

    return response_json


def get_token_data(auth_code, verifier_code):
    url = 'https://account.uipath.com/oauth/token'
    get_refresh_token_data = {
        "grant_type": "authorization_code",
        "code": auth_code,
        "redirect_uri": "https://account.uipath.com/mobile",
        "code_verifier": verifier_code,
        "client_id": "5v7PmPJL6FOGu6RB8I1Y4adLBhIwovQN"
    }

    # Get access_token, id_token, and refresh_token
    response = requests.post(url, data=get_refresh_token_data)
    print(response)
    response_json = json.loads(response.text)
    return response_json


def get_accounts_for_user(id_token):
    url = 'https://platform.uipath.com/cloudrpa/api/getAccountsForUser'
    header = {'Authorization': f'Bearer {id_token}'}

    response = requests.get(url, headers=header)
    print(response)
    response_json = json.loads(response.text)
    return response_json


def get_service_instance_logical_name(account_logical_name, id_token):
    url = f'https://platform.uipath.com/cloudrpa/api/account/{account_logical_name}/getAllServiceInstances'
    header = {'Authorization': f'Bearer {id_token}'}

    response = requests.get(url, headers=header)
    print(response)
    response_json = json.loads(response.text)
    return response_json


def authenticate(auth_code, challenge_code, verifier_code):
    auth_dict = {}
    response_json = get_token_data(auth_code, verifier_code)
    auth_dict['access_token'] = response_json['access_token']
    auth_dict['refresh_token'] = response_json['refresh_token']
    auth_dict['id_token'] = response_json['id_token']

    response_json = get_accounts_for_user(auth_dict['id_token'])
    auth_dict['user_email'] = response_json['userEmail']
    auth_dict['account_name'] = response_json['accounts'][0]['accountName']
    auth_dict['account_logical_name'] = response_json['accounts'][0]['accountLogicalName']

    response_json = get_service_instance_logical_name(auth_dict['account_logical_name'], auth_dict['id_token'])
    auth_dict['service_instance_name'] = response_json[0]['serviceInstanceName']
    auth_dict['service_instance_logical_name'] = response_json[0]['serviceInstanceLogicalName']
    auth_dict['service_url'] = response_json[0]['serviceUrl']

    print(f'refresh_token: {auth_dict["refresh_token"]}')
    print(f'service_instance_logical_name: {auth_dict["service_instance_logical_name"]}')
    print('')
    print(f'user_email: {auth_dict["user_email"]}')
    print(f'account_name: {auth_dict["account_name"]}')
    print(f'account_logical_name: {auth_dict["account_logical_name"]}')
    print(f'service_instance_name: {auth_dict["service_instance_name"]}')
    print(f'service_url: {auth_dict["service_url"]}')
    print('')
    print(f'access_token: {auth_dict["access_token"]}')
    print('')
    print(f'id_token: {auth_dict["id_token"]}')
    print('')
    print('')
    auth_dict['header'] = {
        'Authorization': f'Bearer {auth_dict["access_token"]}',
        'X-UIPATH-TenantName': auth_dict['service_instance_logical_name']
    }
    print('"header" is complete')

    return auth_dict


def make_orchestrator_api_call(access_token, account_logical_name, service_instance_logical_name):
    url = f'https://platform.uipath.com/{account_logical_name}/{service_instance_logical_name}/odata/Settings/UiPath.Server.Configuration.OData.GetLicense'
    header = {
        'Authorization': f'Bearer {access_token}',
        'X-UIPATH-TenantName': service_instance_logical_name
    }

    response = requests.get(url, headers=header)
    print(response)
    response_json = json.loads(response.text)
    return response


def create_text_file(text_file, refresh_token, service_instance_logical_name, header):
    with open(text_file, 'w+') as f:
        f.write(f'refresh_token: {refresh_token}')
        f.write(f'\nservice_instance_logical_name: {service_instance_logical_name}')
        f.write('')
        f.write(f'\n\nHEADER:\n{header}')
        f.write(f'\n\n\nAPI help and examples: "https://postman.uipath.rocks/?version=latest"')
        f.write(f'\nOfficial UiPath API Docs here: "https://docs.uipath.com/orchestrator/v2019/reference#environments-requests"')
        f.write(f'\nFor more information on what this script does, go here: "https://docs.uipath.com/orchestrator/v2019/reference#consuming-cloud-api"')
        

    os.startfile(text_file, 'open')


def get_url(challenge_code):
    print(
        f'https://account.uipath.com/authorize?response_type=code&nonce=b0f368cbc59c6b99ccc8e9b66a30b4a6&state=47441df4d0f0a89da08d43b6dfdc4be2&code_challenge={challenge_code}&code_challenge_method=S256&scope=openid+profile+offline_access+email &audience=https%3A%2F%2Forchestrator.cloud.uipath.com&client_id=5v7PmPJL6FOGu6RB8I1Y4adLBhIwovQN&redirect_uri=https%3A%2F%2Faccount.uipath.com%2Fmobile')

try:
    user_desktop = os.path.join(os.environ["HOMEPATH"], 'Desktop')
    text_file = os.path.join(user_desktop, 'UiPath API Auth Info.txt')
    choice = int(input('"1" for new authentication.\n"2" for refresh.\nEnter: '))
    if choice == 2:
        second_choice = int(input('"1" to input manually.\n"2" to use a file.\nEnter: '))
        if second_choice == 2:
            root = tkinter.Tk()
            root.withdraw() # Close the root window
            root.overrideredirect(True)
            root.geometry('0x0+0+0')
            root.deiconify()
            root.lift()
            root.focus_force()
            filename = askopenfilename(parent=root)
            root.destroy()
            with open(filename, 'r') as f:
                for line in f.readlines():
                    if 'refresh_token' in line:
                        refresh_token = line.split('refresh_token: ')[1].strip('\n')
                    if 'service_instance_logical_name' in line:
                        service_instance_logical_name = line.split('service_instance_logical_name: ')[1].strip('\n')

        elif second_choice == 1:
            refresh_token = input('Refresh Token: ')
            service_instance_logical_name = input('Service Instance Logical Name: ')

        response_json = refresh_token_data(refresh_token)
        access_token = response_json['access_token']

        header = {
            'Authorization': f'Bearer {access_token}',
            'X-UIPATH-TenantName': service_instance_logical_name
        }
        create_text_file(text_file,
                         refresh_token,
                         service_instance_logical_name,
                         header)
        print('Success')
        time.sleep(5)

    elif choice == 1:
        print('\nGo to "https://repl.it/languages/nodejs" and paste in the below.')
        print('')
        print('''
        function base64URLEncode(str) {
          return str.toString('base64')
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/=/g, '');
        }

        function sha256(buffer) {
          return crypto.createHash('sha256').update(buffer).digest();
        }

        console.log("Generating challenge and Verifier");
        var cryptoResult = crypto.randomBytes(32);
        var verifier = base64URLEncode(cryptoResult);
        var challenge = base64URLEncode(sha256(verifier));
        console.log("Code challenge: " + challenge)
        console.log("Code verifier: " + verifier);

        ''')
        print('\nHit "Run", and copy the requested information below.\n')
        
        challenge_code = input('Code Challenge: ')
        verifier_code = input('Code Verifier: ')
        print('')
        get_url(challenge_code)
        print('')
        print('Paste that URL into a browser, \nAuthenticate if needed,\nCopy the new url and paste it below.')
        print('')
        while True:
            try:
                auth_code = input('URL: ').split('code=')[1].split('&state')[0]
                if auth_code:
                    break
            except IndexError:
                print('URL is missing or incorrect. Please paste the entire URL.')

        auth_dict = authenticate(auth_code, challenge_code, verifier_code)

        response_json = make_orchestrator_api_call(auth_dict['access_token'],
                                                   auth_dict['account_logical_name'],
                                                   auth_dict['service_instance_logical_name'])
        print(response_json.text)
        print('')
        print('')
        print('This header must be included in all API requests.')
        print('')
        print(auth_dict['header'])

        print('')
        print('Testing in 5 seconds.')
        time.sleep(5)
        print('Testing')
        print('')
        sessions_test = requests.get('https://platform.uipath.com/odata/Sessions?', headers=auth_dict['header'])
        sessions_test = json.loads(sessions_test.text)
##        print(sessions_test)
        try:
            if sessions_test['message'] == 'You are not authorized!':
                print('Account used is not authorized to use that API command (Getting a machine list).')
            else:
                for item, value in sessions_test['value'][0].items():
                print(f'{item}: {value}')
        except:
            print('Test skipped. Here\'s the traceback.')
            traceback.print_exc()
            

        print('')
        print('')
        print(f'Creating text file at "{text_file}"')
        create_text_file(text_file,
                         auth_dict["refresh_token"],
                         auth_dict["service_instance_logical_name"],
                         auth_dict["header"])
        print('Success')
        time.sleep(5)
except KeyboardInterrupt:
    pass
except:
    print('Something went wrong.')
    traceback.print_exc()
    print('Closing in 20 seconds.')
    time.sleep(20)





